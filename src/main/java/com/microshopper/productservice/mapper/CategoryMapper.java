package com.microshopper.productservice.mapper;

import com.microshopper.productservice.model.Category;
import com.microshopper.productservice.web.dto.CategoryDto;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

  public CategoryDto mapToCategoryDto(Category object) {

    CategoryDto categoryDto = new CategoryDto();
    categoryDto.setId(object.getId());
    categoryDto.setName(object.getName());
    categoryDto.setDescription(object.getDescription());

    return categoryDto;
  }

  public Category mapFromCategoryDto(CategoryDto object) {

    Category category = new Category();
    category.setName(object.getName());
    category.setDescription(object.getDescription());

    return category;
  }
}
