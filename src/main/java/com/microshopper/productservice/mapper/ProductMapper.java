package com.microshopper.productservice.mapper;

import com.microshopper.productservice.model.Product;
import com.microshopper.productservice.web.dto.AddProductDto;
import com.microshopper.productservice.web.dto.ProductDto;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

  public ProductDto mapToProductDto(Product object) {

    ProductDto productDto = new ProductDto();
    productDto.setId(object.getId());
    productDto.setName(object.getName());
    productDto.setPrice(object.getPrice());
    productDto.setDescription(object.getDescription());
    productDto.setQuantity(object.getQuantity());
    productDto.setManufacturerId(object.getManufacturerId());

    return productDto;
  }

  public Product mapFromProductDto(ProductDto object) {

    Product product = new Product();
    product.setName(object.getName());
    product.setDescription(object.getDescription());
    product.setPrice(object.getPrice());
    product.setQuantity(object.getQuantity());
    product.setManufacturerId(object.getManufacturerId());

    return product;
  }

  public Product mapFromAddProductDto(AddProductDto addProductDto) {

    Product product = new Product();
    product.setName(addProductDto.getName());
    product.setPrice(addProductDto.getPrice());
    product.setDescription(addProductDto.getDescription());
    product.setQuantity(addProductDto.getQuantity());
    product.setManufacturerId(addProductDto.getManufacturerId());

    return product;
  }
}
