package com.microshopper.productservice.repository;

import com.microshopper.productservice.model.Product;
import com.microshopper.productservice.model.QProduct;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends
  JpaRepository<Product, Long>,
  QuerydslPredicateExecutor<Product>,
  QuerydslBinderCustomizer<QProduct> {

  List<Product> getAllByCategoryId(Long categoryId);

  List<Product> getAllByManufacturerId(Long manufacturerId);

  @Override
  default void customize(QuerydslBindings querydslBindings, QProduct qProduct) {

    querydslBindings.bind(qProduct.name).first(StringExpression::containsIgnoreCase);
    querydslBindings.bind(qProduct.manufacturerId).first(NumberExpression::eq);
    querydslBindings.bind(qProduct.category.id).as("categoryId").first(NumberExpression::eq);
  }
}
