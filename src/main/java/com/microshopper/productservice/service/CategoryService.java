package com.microshopper.productservice.service;

import com.microshopper.productservice.web.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

  List<CategoryDto> getAllCategories();

  CategoryDto getCategory(Long id);

  CategoryDto addCategory(CategoryDto categoryDto);

  void updateCategory(Long id, CategoryDto categoryDto);

  void deleteCategory(Long id);
}
