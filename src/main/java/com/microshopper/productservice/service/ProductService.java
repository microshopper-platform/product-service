package com.microshopper.productservice.service;

import com.microshopper.productservice.web.dto.AddProductDto;
import com.microshopper.productservice.web.dto.ProductDto;
import com.querydsl.core.types.Predicate;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAllProducts();

    List<ProductDto> getAllCategoryProducts(Long categoryId);

    List<ProductDto> getAllManufacturerProducts(Long manufacturerId);

    ProductDto getProduct(Long id);

    ProductDto addProduct(AddProductDto addProductDto);

    void updateProduct(Long id, ProductDto productDto);

    void deleteProduct(Long id);

    List<ProductDto> searchProducts(Predicate predicate);
}
