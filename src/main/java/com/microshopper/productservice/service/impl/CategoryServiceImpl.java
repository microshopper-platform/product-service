package com.microshopper.productservice.service.impl;

import com.microshopper.productservice.exception.CategoryNotFoundException;
import com.microshopper.productservice.mapper.CategoryMapper;
import com.microshopper.productservice.model.Category;
import com.microshopper.productservice.repository.CategoryRepository;
import com.microshopper.productservice.service.CategoryService;
import com.microshopper.productservice.web.dto.CategoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;
  private final CategoryMapper categoryMapper;

  @Autowired
  public CategoryServiceImpl(
    CategoryRepository categoryRepository,
    CategoryMapper categoryMapper) {

    this.categoryRepository = categoryRepository;
    this.categoryMapper = categoryMapper;
  }

  @Override
  public List<CategoryDto> getAllCategories() {

    List<Category> categories = categoryRepository.findAll();

    return categories
      .stream()
      .map(categoryMapper::mapToCategoryDto).collect(Collectors.toList());
  }

  @Override
  public CategoryDto getCategory(Long id) {

    Category category = categoryRepository.getOne(id);

    return categoryMapper.mapToCategoryDto(category);
  }

  @Override
  public CategoryDto addCategory(CategoryDto categoryDto) {

    Category category = categoryMapper.mapFromCategoryDto(categoryDto);
    Category newCategory = categoryRepository.save(category);
    return categoryMapper.mapToCategoryDto(newCategory);
  }

  @Override
  public void updateCategory(Long id, CategoryDto categoryDto) {

    Optional<Category> categoryToBeUpdated = categoryRepository.findById(id);
    categoryToBeUpdated.ifPresentOrElse(category -> {
      category.setDescription(categoryDto.getDescription());
      categoryRepository.save(category);
    }, () -> { throw new CategoryNotFoundException("Category update failed. Category with id: " + id + " not found.");
    });
  }

  @Override
  public void deleteCategory(Long id) {

    categoryRepository.deleteById(id);
  }
}
