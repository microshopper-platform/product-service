package com.microshopper.productservice.service.impl;

import com.microshopper.productservice.exception.CategoryNotFoundException;
import com.microshopper.productservice.exception.ProductNotFoundException;
import com.microshopper.productservice.mapper.CategoryMapper;
import com.microshopper.productservice.mapper.ProductMapper;
import com.microshopper.productservice.model.Category;
import com.microshopper.productservice.model.Product;
import com.microshopper.productservice.repository.CategoryRepository;
import com.microshopper.productservice.repository.ProductRepository;
import com.microshopper.productservice.service.ProductService;
import com.microshopper.productservice.web.dto.AddProductDto;
import com.microshopper.productservice.web.dto.CategoryDto;
import com.microshopper.productservice.web.dto.ProductDto;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;
  private final CategoryRepository categoryRepository;
  private final ProductMapper productMapper;
  private final CategoryMapper categoryMapper;

  @Autowired
  public ProductServiceImpl(
    ProductRepository productRepository,
    CategoryRepository categoryRepository,
    ProductMapper productMapper,
    CategoryMapper categoryMapper) {

    this.productRepository = productRepository;
    this.categoryRepository = categoryRepository;
    this.productMapper = productMapper;
    this.categoryMapper = categoryMapper;
  }

  @Override
  public List<ProductDto> getAllProducts() {

    List<Product> products = productRepository.findAll();

    return products.stream().map(product -> {

      ProductDto productDto = productMapper.mapToProductDto(product);
      CategoryDto categoryDto = categoryMapper.mapToCategoryDto(product.getCategory());
      productDto.setCategory(categoryDto);

      return productDto;
    }).collect(Collectors.toList());
  }

  @Override
  public List<ProductDto> getAllCategoryProducts(Long categoryId) {

    List<Product> products = productRepository.getAllByCategoryId(categoryId);

    return products.stream().map(productMapper::mapToProductDto).collect(Collectors.toList());
  }

  @Override
  public List<ProductDto> getAllManufacturerProducts(Long manufacturerId) {

    List<Product> products = productRepository.getAllByManufacturerId(manufacturerId);

    return products.stream().map(product -> {

      ProductDto productDto = productMapper.mapToProductDto(product);
      CategoryDto categoryDto = categoryMapper.mapToCategoryDto(product.getCategory());
      productDto.setCategory(categoryDto);

      return productDto;
    }).collect(Collectors.toList());
  }

  @Override
  public ProductDto getProduct(Long id) {

    Product product = productRepository.getOne(id);

    ProductDto productDto = productMapper.mapToProductDto(product);
    CategoryDto categoryDto = categoryMapper.mapToCategoryDto(product.getCategory());
    productDto.setCategory(categoryDto);

    return productDto;
  }

  @Override
  public ProductDto addProduct(AddProductDto addProductDto) {

    Product product = productMapper.mapFromAddProductDto(addProductDto);

    Optional<Category> categoryOptional = categoryRepository.findById(addProductDto.getCategoryId());
    if (categoryOptional.isEmpty()) {
      throw new CategoryNotFoundException("Add new product failed. Category with id: " + addProductDto.getCategoryId() + " not found." );
    }
    product.setCategory(categoryOptional.get());

    Product newProduct = productRepository.save(product);
    return productMapper.mapToProductDto(newProduct);
  }

  @Override
  public void updateProduct(Long id, ProductDto productDto) {

    Optional<Product> productToBeUpdated = productRepository.findById(id);
    productToBeUpdated.ifPresentOrElse(product -> {
      product.setName(productDto.getName());
      product.setPrice(productDto.getPrice());
      product.setDescription(productDto.getDescription());
      product.setQuantity(productDto.getQuantity());
      productRepository.save(product);
    }, () -> { throw new ProductNotFoundException("Product update failed. Product with id: " + id + " not found!");
    });
  }

  @Override
  public void deleteProduct(Long id) {

    productRepository.deleteById(id);
  }

  @Override
  public List<ProductDto> searchProducts(Predicate predicate) {
    Iterable<Product> products = productRepository.findAll(predicate);
    List<ProductDto> productDtos = new ArrayList<>();
    products.forEach(product -> {

      ProductDto productDto = productMapper.mapToProductDto(product);
      CategoryDto categoryDto = categoryMapper.mapToCategoryDto(product.getCategory());
      productDto.setCategory(categoryDto);

      productDtos.add(productDto);
    });

    return productDtos;
  }
}
