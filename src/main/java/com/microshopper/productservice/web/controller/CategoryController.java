package com.microshopper.productservice.web.controller;

import com.microshopper.productservice.service.CategoryService;
import com.microshopper.productservice.web.dto.CategoryDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "category", description = "The Category API")
public class CategoryController {

  private static final String BASE_URL = "/api/categories";

  private final CategoryService categoryService;

  @Autowired
  public CategoryController(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @GetMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.OK)
  public List<CategoryDto> getAllCategories() {
    return categoryService.getAllCategories();
  }

  @GetMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public CategoryDto getCategory(@PathVariable Long id) {
    return categoryService.getCategory(id);
  }

  @PostMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.CREATED)
  public CategoryDto addCategory(@RequestBody CategoryDto categoryDto) {

    return categoryService.addCategory(categoryDto);
  }

  @PutMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateCategory(@PathVariable Long id, @RequestBody CategoryDto categoryDto) {

    categoryService.updateCategory(id, categoryDto);
  }

  @DeleteMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void deleteCategory(@PathVariable Long id) {

    categoryService.deleteCategory(id);
  }
}
