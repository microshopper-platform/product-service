package com.microshopper.productservice.web.controller;

import com.microshopper.productservice.model.Product;
import com.microshopper.productservice.service.ProductService;
import com.microshopper.productservice.web.dto.AddProductDto;
import com.microshopper.productservice.web.dto.ProductDto;
import com.querydsl.core.types.Predicate;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "product", description = "The Product API")
public class ProductController{

  private static final String BASE_URL = "/api/products";

  private final ProductService productService;

  @Autowired
  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @GetMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.OK)
  public List<ProductDto> getAllProducts() {
    return productService.getAllProducts();
  }

  @GetMapping(value = BASE_URL + "/manufacturer/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public List<ProductDto> getAllManufacturerProducts(@PathVariable(name = "id") Long manufacturerId) {
    return productService.getAllManufacturerProducts(manufacturerId);
  }

  @GetMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public ProductDto getProduct(@PathVariable Long id) {
    return productService.getProduct(id);
  }

  @PostMapping(value = BASE_URL)
  @ResponseStatus(value = HttpStatus.CREATED)
  public ProductDto addProduct(@RequestBody AddProductDto addProductDto) {
    return productService.addProduct(addProductDto);
  }

  @PutMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void updateProduct(@PathVariable Long id, @RequestBody ProductDto productDto) {
    productService.updateProduct(id, productDto);
  }

  @DeleteMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void deleteProduct(@PathVariable Long id) {
    productService.deleteProduct(id);
  }

  @GetMapping(value = BASE_URL + "/search")
  @ResponseStatus(value = HttpStatus.OK)
  public List<ProductDto> searchProducts(
    @Parameter(hidden = true) @QuerydslPredicate(root = Product.class) Predicate predicate,
    @RequestParam(required = false) String name,
    @RequestParam(required = false) Long manufacturerId,
    @RequestParam(required = false) Long categoryId ) {

    return productService.searchProducts(predicate);
  }
}
