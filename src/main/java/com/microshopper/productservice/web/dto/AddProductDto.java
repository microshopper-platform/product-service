package com.microshopper.productservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "AddProduct")
public class AddProductDto {

  private String name;

  private Integer price;

  private Long categoryId;

  private String description;

  private Integer quantity;

  private Long manufacturerId;
}
