package com.microshopper.productservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName(value = "Category")
public class CategoryDto {

  private Long id;

  private String name;

  private String description;
}
