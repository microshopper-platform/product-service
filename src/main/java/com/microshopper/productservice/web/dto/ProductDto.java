package com.microshopper.productservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName("Product")
public class ProductDto {

  private Long id;

  private String name;

  private Integer price;

  private CategoryDto category;

  private String description;

  private Integer quantity;

  private Long manufacturerId;
}
