CREATE TABLE product (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    price integer NOT NULL,
    category_id integer,
    description character varying(2048),
    quantity integer NOT NULL,
    manufacturer_id integer,
    PRIMARY KEY (id)
);

CREATE TABLE category (
    id serial NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(2048),
    PRIMARY KEY (id)
);

ALTER TABLE product ADD CONSTRAINT FK_PRODUCT_CATEGORY FOREIGN KEY (category_id) REFERENCES category (id);

